package com.muhardin.endy.belajar.ssl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarSslApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarSslApplication.class, args);
	}

}
