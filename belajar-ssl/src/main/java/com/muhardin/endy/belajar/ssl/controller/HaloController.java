package com.muhardin.endy.belajar.ssl.controller;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HaloController {

    @GetMapping("/api/halo")
    public Map<String, Object> info(){
        Map<String, Object> data = new HashMap<>();
        data.put("waktu", LocalDateTime.now());
        return data;
    }
}
