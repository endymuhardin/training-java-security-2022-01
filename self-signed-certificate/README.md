# Membuat Self Signed Certificate #

Ada 2 pihak yang terlibat :

1. Certificate Authority
2. Website demo.endy.muhardin.com

## Website ##

1. Membuat keypair

    ```
    openssl genrsa -aes256
    ```

2. Membuat certificate signing request

    ```
    openssl req -new -key private-key.txt -out demo.endy.muhardin.com.csr
    ```

    Jawab pertanyaan yang diajukan:

    ```
    Enter pass phrase for private-key.txt: abcd1234
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) []:id
    State or Province Name (full name) []:DKI Jakarta
    Locality Name (eg, city) []:Jakarta
    Organization Name (eg, company) []:PT Maju Terus
    Organizational Unit Name (eg, section) []:IT
    Common Name (eg, fully qualified host name) []:demo.endy.muhardin.com
    Email Address []:demo@yopmail.com

    Please enter the following 'extra' attributes
    to be sent with your certificate request
    A challenge password []:
    ```

3. Membuat key storage untuk menyimpan certificate yang sudah di-sign

## Certificate ##

1. Membuat keypair

    ```
    openssl genrsa -aes256
    ```

2. Membuat CA certificate

    ```
    openssl req -new -x509 -days 3650 -key private-key-ca.txt > ca-certificate.txt
    ```

3. Melakukan signing terhadap CSR

    ```
    openssl x509 -req -CA ca-certificate.txt -CAkey private-key-ca.txt -in demo.endy.muhardin.com/demo.endy.muhardin.com.csr -out demo.endy.muhardin.com/demo.endy.muhardin.com.crt -days 365 -CAcreateserial
    ```

4. Menampilkan isi certificate

    ```
    openssl x509 -in demo.endy.muhardin.com.crt -text
    ```

## Menyimpan Certificate dalam PKCS12 ##

1. Apabila certificate kita di-sign oleh intermediate certificate (bukan certificate Root CA), maka gabungkan dulu semua intermediate tersebut. Dimulai dari yang dipakai sign certificate kita, sampai ke certificate Root.

2. Pastikan certificate kita rantai signaturenya tersambung sampai ke certificate Root CA

    ```
    openssl verify -CAfile ../certificate-authority/ca-certificate.txt demo.endy.muhardin.com.crt
    ```

3. Simpan certificate chain, certificate kita, private key kita ke dalam satu file PKCS12

    ```
    openssl pkcs12 -export -chain \
    -inkey private-key.txt \
    -in demo.endy.muhardin.com.crt \
    -name "demo.endy.muhardin.com" \
    -CAfile ../certificate-authority/ca-certificate.txt \
    -caname "Root CA" \
    -out demo.endy.muhardin.com.p12
    ```

4. Menampilkan isi database PKCS12

    ```
    openssl pkcs12 -nokeys -info -in demo.endy.muhardin.com.p12
    ```